package NeuralTranslationLanguageModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import Processing.ProcessQueries;

public class Translation {
	private static final int K = 5; 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<String, HashMap<String, Vector<Double>>> embeddings = new HashMap<String, HashMap<String, Vector<Double>>>();
		HashMap<Integer, String[]> queries = ProcessQueries.processPostQuery("topics51-200.trec");
		Vector<String> stopwords = new Vector<String>();
		
		//Get Stopwords, might speed up calculations slightly
		try {
			BufferedReader br = new BufferedReader(new FileReader("stopword-list.txt"));
			String line;
			//Loop through each line in the document collection
			while ((line = br.readLine()) != null) {
				stopwords.add(line);
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Comment and decomment to do these one at a time. Memory overhead too large.
		writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_cbow_s300_w5_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/CBOW_W5.txt");
		
	    writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_cbow_s300_w10_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/CBOW_W10.txt");
		
		writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_cbow_s300_w15_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/CBOW_W15.txt");
		
		writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_skipgram_s300_w5_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/skipgram_W5.txt");
		
		writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_skipgram_s300_w10_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/skipgram_W10.txt");
		
		writeExpansionToFile(embeddings, queries, stopwords, "C:/Embeddings/vectors_ap8889_skipgram_s300_w15_neg20_hs0_sam1e-4_iter5.txt", "C:/cab431-project/ExpandedQueries/skipgram_W15.txt");
		
	}
	
	/**
	 * Computes cosine similarity for all query words and writes them to a new query file in TREC format
	 * @param embeddings - A HashMap containing all the embeddings for a collection
	 * @param queries - The original query terms
	 * @param stopwords - A list of stopwords
	 * @param filename - Filename containing the embeddings
	 * @param fileToWriteTo - File to write expanded Queries to
	 */
	private static void writeExpansionToFile(HashMap<String, HashMap<String, Vector<Double>>> embeddings,
			HashMap<Integer, String[]> queries, Vector<String> stopwords, String filename, String fileToWriteTo) {
		HashMap<String, Vector<Double>> embeddingVector;
		HashMap<Integer, HashMap<String, HashMap<String, Double>>> queryNumWordVocabProb = new HashMap<Integer, HashMap<String, HashMap<String, Double>>>();
		HashMap<String, HashMap<String, Double>> queryWordVocabProb = new HashMap<String, HashMap<String, Double>>();
		HashMap<String, Double> temp = new HashMap<String, Double>();
	
		double prob;
		embeddingVector = vocabVectors(filename);
		embeddings.put(filename, embeddingVector);
		System.out.println(filename);
		//Run through each query
		for(int query = 51; query < 201; query++) {
			System.out.println(query);
			String queryWords[] = queries.get(query);
			//Run through each query term
			for(int j = 0; j < queryWords.length; j++) {
				//Check if the query term is in vocab
				if(embeddings.get(filename).containsKey(queryWords[j])) {
					//Run through each word in the vocabulary 
					for(String vocabWord : embeddings.get(filename).keySet()) {
						//Check if word is stopword and not comparing word to itself
						if(!stopwords.contains(vocabWord) && !queryWords[j].equals(vocabWord)) {
							prob = calculateCosine(embeddings.get(filename).get(queryWords[j]), 
										embeddings.get(filename).get(vocabWord));
							//Try and only get most relevant to reduce memory overhead
							if(prob > -0.009) {
								temp.put(vocabWord, prob);
							}
						}
					}
					queryWordVocabProb.put(queryWords[j], temp);
					temp = new HashMap<String, Double>();
				}
			}
			queryNumWordVocabProb.put(query, queryWordVocabProb);
			queryWordVocabProb = new HashMap<String, HashMap<String, Double>>();
		}
		
		HashMap<Integer, HashMap<String, String[]>> queryNumExpansion = new HashMap<Integer, HashMap<String, String[]>>();
		HashMap<String, String[]> temp2 = new HashMap<String, String[]>();
		double highestVal = 0;
		String[] topWords = new String[K];
		Boolean alreadyHaveWord = false;
		System.out.println("Expanding query");
		for(Integer query : queryNumWordVocabProb.keySet()) {
			System.out.println(query);
			for(String queryWord : queryNumWordVocabProb.get(query).keySet()) {
				//Get top 5 translations
				for(int i = 0; i < topWords.length; i++) {
					for(String vocabWord : queryNumWordVocabProb.get(query).get(queryWord).keySet()) {
						//Check if it is a higher value
						if(queryNumWordVocabProb.get(query).get(queryWord).get(vocabWord) > highestVal) {
							//Check the word if it is already been selected as an expansion term
							for(int j = 0; j < i; j++) {
								if(topWords[j].equals(vocabWord)) {
									alreadyHaveWord = true;
								}
							}
							if(!alreadyHaveWord) {
								highestVal = queryNumWordVocabProb.get(query).get(queryWord).get(vocabWord);
								topWords[i] = vocabWord;
							}
						}
					}
					highestVal = 0;
					alreadyHaveWord = false;
				}
				temp2.put(queryWord, topWords);
				topWords = new String[K];
			}
			queryNumExpansion.put(query, temp2);
			temp2 = new HashMap<String, String[]>();
		}
		
		System.out.println("Writing expanded queries to file");
		//Write expanded queries to file
		try {
			FileWriter fr = new FileWriter(fileToWriteTo);
			BufferedWriter bufferedWriter = new BufferedWriter(fr);
			for(Integer query : queryNumExpansion.keySet()) {
				bufferedWriter.write("<top>");
            	bufferedWriter.newLine();
            	bufferedWriter.write("<num>" + (query) + "</num><title>");
            	bufferedWriter.newLine();
				for(String queryWord : queryNumExpansion.get(query).keySet()) {
					bufferedWriter.write(queryWord + " ");
					//Write top 5 expansion terms for each query term to file
					for(String expansionTerm : queryNumExpansion.get(query).get(queryWord)) {
						bufferedWriter.write(expansionTerm + " ");
					}
				}
				bufferedWriter.newLine();
    			bufferedWriter.write("</title>");
    			bufferedWriter.newLine();
    			bufferedWriter.write("</top>");
    			bufferedWriter.newLine();
			}
			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		e.printStackTrace();
		}
		System.out.println("Done!");
	}
	
	/**
	 * Calculates Neural Translation Language Model
	 * @param queryWordVec - vector representation of query word embedding
	 * @param docWordVec - vector representation of doc word embedding
	 * @param vocab - entire vocabulary of vector embedding's
	 * @return - score
	 */
	@Deprecated
	public static double calculateTranslationProb(Vector<Double> queryWordVec, Vector<Double> docWordVec, HashMap<String, Vector<Double>> vocab) {
		double probability = 0;
		double normalisation = summEntireVocab(docWordVec, vocab); 
		probability = calculateCosine(queryWordVec, docWordVec)/normalisation;
		return probability;
	}
	
	/**
	 * Calculates the cosine value of two vectors
	 * @param queryWordVec - vector representation of query word embedding
	 * @param docWordVec - vector representation of doc word embedding 
	 * @return - the cosine value
	 */
	private static double calculateCosine(Vector<Double> queryWordVec, Vector<Double> docWordVec) {
		double cosine = 0;
		List<Double> docTimesQuery = new ArrayList<Double>();
		List<Double> docSquared = new ArrayList<Double>();
		List<Double> querySquared = new ArrayList<Double>();
		//Get the figures to sum
		for(int i = 0; i < queryWordVec.size(); i++) {
			docTimesQuery.add(queryWordVec.get(i) * docWordVec.get(i));
			docSquared.add(docWordVec.get(i) * docWordVec.get(i));
			querySquared.add(queryWordVec.get(i) * queryWordVec.get(i));
		}
		//Perform cosine similarity
		cosine = summation(docTimesQuery) / Math.sqrt(summation(docSquared) * summation(querySquared)); 
		docTimesQuery.clear();
		docSquared.clear();
		querySquared.clear();
		return cosine;
	}
	
	/**
	 * Calculates the sum of an array of values
	 * @param values - an array of double elements
	 * @return - the total sum
	 */
	private static double summation(List<Double> values) {
		double total = 0;
		for(Double value : values) {
			total += value;
		}
		return total;
	}
	
	/**
	 * Sums the cosine of a  word against every word in the vocabulary
	 * @param wordVector - vector representation of doc word embedding
	 * @param vocab - a HashMap containing every vector embedding in the vocabulary
	 * @return - the total sum
	 */
	@Deprecated
	private static double summEntireVocab(Vector<Double> wordVector, HashMap<String, Vector<Double>> vocab) {
		double total = 0;
		double cosine = 0;
		int i = 0;
		List<Double> values = new ArrayList<Double>();
		for(String word : vocab.keySet()) {
			cosine = calculateCosine(wordVector, vocab.get(word));
			values.add(cosine);
			i++;
		}
		total = summation(values);
		return total;
	}
	
	/**
	 * Import the embedding's from a text file into a HashMap
	 * @param filename - file containing the embedding's
	 * @return - a HashMap containing all the vector embedding's
	 */
	private static HashMap<String, Vector<Double>> vocabVectors(String filename) {
		Vector<Double> temp = new Vector<Double>();
		HashMap<String, Vector<Double>> vocab = new HashMap<String, Vector<Double>>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			String[] splitStr;
			//Read/skip first line of text in embedding file, does not contain embedding
			br.readLine();
			while((line = br.readLine()) != null) {
				splitStr = line.split("\\s+");
				for(int i = 1; i < splitStr.length; i++) {
					temp.add(Double.parseDouble(splitStr[i]));
				}
				vocab.put(splitStr[0], temp);
				temp = new Vector<Double>();
			}
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		return vocab;
	}
}
