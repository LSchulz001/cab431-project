package Processing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class ProcessQueries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] queryFiles = {"topics.51-100.TREC", "topics.101-150", "topics.151-200"};
		HashMap<Integer, String> queries = processQuery("topics.51-100");
		HashMap<Integer, String> queries2 = processQuery("topics.101-150");
		HashMap<Integer, String> queries3 = processQuery("topics.151-200");
		HashMap<Integer, HashMap<Integer, String>> allQueries = new HashMap<Integer, HashMap<Integer, String>>();
		allQueries.put(0, queries);
		allQueries.put(1, queries2);
		allQueries.put(2, queries3);
		writeQueryToFile(allQueries, "topics51-200.trec", 50);
	}
	
	/**
	 * Imports queries from a file and adds them to a HashMap
	 * @param filename - file containing the queries
	 * @return - A HashMap containing the queries
	 */
	public static HashMap<Integer, String> processQuery(String filename) {
		HashMap<Integer, String> queries = new HashMap<Integer, String>();
		int queryNum = 0;
		String query = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			//Loop through each line in the document collection
			while ((line = br.readLine()) != null) {
				String[] splitStr = line.trim().split("\\s+");
				if(line.contains("<num>")) {
					queryNum = Integer.parseInt(splitStr[2]);
					query = "";
				} else if(line.contains("<title>")) {
					for(int i = 2; i < splitStr.length; i++) {
						query = query.concat(splitStr[i] + " ");
					}
					//System.out.println(queryNum + " " + query);
					queries.put(queryNum, query);
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return queries;
	}
	
	/**
	 * Processes a query after it has been formatted into Trec format.
	 * @param filename - file containing the queries.
	 * @return - a HashMap containing the queries.
	 */
	public static HashMap<Integer, String[]> processPostQuery(String filename) {
		HashMap<Integer, String[]> queries = new HashMap<Integer, String[]>();
		int queryNum = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			//Loop through each line in the document collection
			while ((line = br.readLine()) != null) {
				//Determine if the current line is a query declaration or open/close tag, otherwise split the query into words
				if(line.contains("<num>")) { 
					String[] splitQuery = line.split(">");
					String[] splitQueryNum = splitQuery[1].split("<");
					queryNum = Integer.parseInt(splitQueryNum[0]);
				} else if(line.contains("title>") || line.contains("top>")) {
					//Do nothing, skip line
				} else {
					String[] splitStr = line.toLowerCase().trim().split("\\s+");
					queries.put(queryNum, splitStr);
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return queries;
	}
	
	/**
	 * Combines several HashMaps containing queries into one file.
	 * @param queries - HashMap containing another HashMap with the queries for the respective file.
	 * @param filename - file to write the queries to.
	 * @param firstNum - number of the first query. 
	 */
	public static void writeQueryToFile(HashMap<Integer ,HashMap<Integer, String>> queries, String filename, int firstNum) {
		try {
			FileWriter fr = new FileWriter(filename);
			BufferedWriter bufferedWriter = new BufferedWriter(fr);
			int queryNum = 0;
			for(Integer queryfile:queries.keySet()) {
				System.out.println(queries.get(queryfile).size());
				if(queryfile == 0) {
					queryNum = 51;
				} else if(queryfile == 1) {
					queryNum = 101;
				} else {
					queryNum = 151;
				}
					
				for(int i = 0; i < queries.get(queryfile).size(); i++) {
					bufferedWriter.write("<top>");
	            	bufferedWriter.newLine();
	            	bufferedWriter.write("<num>" + (i + queryNum) + "</num><title>");
	            	bufferedWriter.newLine();
	            	System.out.println(i + queryNum);
	    			bufferedWriter.write(queries.get(queryfile).get(i + queryNum));
	    			bufferedWriter.newLine();
	    			bufferedWriter.write("</title>");
	    			bufferedWriter.newLine();
	    			bufferedWriter.write("</top>");
	    			bufferedWriter.newLine();
				}
			}
			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
