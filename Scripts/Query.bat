@echo off
title Queries
REM run the following queries against the indexed collection using TF-IDF and BM25
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/cab431-project/Code/topics51-200.trec
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/cab431-project/Code/topics51-200.TREC

echo done
pause