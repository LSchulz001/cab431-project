@echo off
title Queries Expanded
REM Skipgram
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W5.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W5.txt

call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W10.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W10.txt

call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W15.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/CBOW_W15.txt

call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W5.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W5.txt

call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W10.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W10.txt

call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=TF_IDF -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W15.txt
call C:/terrier-core-4.2/bin/trec_terrier.bat -r -Dtrec.model=BM25 -Dtrec.topics=C:/Users/%USERNAME%/Dropbox/CAB431/Project/Queries/skipgram_W15.txt

echo done
pause