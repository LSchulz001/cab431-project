@echo off
title Evaluation Expanded
REM perform MAP evaluation on the results obtained
echo CBOW W5
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_26.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_9.res
echo CBOW W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_27.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_10.res
echo CBOW W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_28.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_12.res
echo Skipgram W5
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_29.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_13.res
echo Skipgram W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_30.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_15.res
echo Skipgram W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_31.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_16.res
pause

REM perform GMAP evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_26.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_9.res
echo CBOW W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_27.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_10.res
echo CBOW W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_28.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_12.res
echo Skipgram W5
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_29.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_13.res
echo Skipgram W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_30.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_15.res
echo Skipgram W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_31.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_16.res
pause

REM perform P@10 evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_26.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_9.res
echo CBOW W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_27.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_10.res
echo CBOW W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_28.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_12.res
echo Skipgram W5
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_29.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_13.res
echo Skipgram W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_30.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_15.res
echo Skipgram W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_31.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_16.res
pause

REM perform nDCG@10 evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_26.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_9.res
echo CBOW W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_27.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_10.res
echo CBOW W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_28.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_12.res
echo Skipgram W5
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_29.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_13.res
echo Skipgram W10
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_30.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_15.res
echo Skipgram W15
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_31.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_16.res
echo done
pause