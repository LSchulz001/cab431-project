@echo off
title Evaluation
REM perform MAP evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_0.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_1.res

REM perform GMAP evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_0.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m gm_map C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_1.res

REM perform P@10 evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_0.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m P C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_1.res

REM perform nDCG@10 evaluation on the results obtained
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/TF_IDF_0.res
call C:/trec_eval/_/trec_eval.9.0/trec_eval -q -m ndcg_cut.10 C:/Users/%USERNAME%/Dropbox/CAB431/Project/trec123.51-200.ap8889.qrels C:/Users/%USERNAME%/Dropbox/CAB431/Project/Results/BM25b0.75_1.res

echo done
pause